package com.example.cloud.avro.configuration;

import org.springframework.cloud.stream.schema.avro.AvroSchemaMessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MessageConverter;
import za.brinkc.schema.pojo.DomainEvent;

@Configuration
public class ApplicationConfiguration {
    @Bean
    public MessageConverter classificationMessageConverter() {
        AvroSchemaMessageConverter converter = new AvroSchemaMessageConverter();
        converter.setSchema(DomainEvent.SCHEMA$);
        return converter;
    }
}
