package com.example.cloud.avro.infrastructure;

import com.example.cloud.avro.service.ActionService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Component;
import za.brinkc.schema.pojo.DomainEvent;

@Slf4j
@Component
@EnableBinding(Channels.class)
public class EventHandler {

    private ActionService actionService;

    public EventHandler(final ActionService actionService) {
        this.actionService = actionService;
    }

    @StreamListener(Channels.EVENTS)
    public void receive(final DomainEvent event) {
        log.debug("Received event [{}]", event);

        actionService.process(event);
    }
}
