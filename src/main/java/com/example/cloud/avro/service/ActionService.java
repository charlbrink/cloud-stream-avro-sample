package com.example.cloud.avro.service;

import com.example.cloud.avro.infrastructure.Channels;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Service;
import za.brinkc.schema.pojo.DomainEvent;

@Slf4j
@Service
public class ActionService {

    private final Channels channels;

    public ActionService(Channels channels) {
        this.channels = channels;
    }

    public void process(DomainEvent event) {
        log.info("Received {}", event);
        actioned(event);
    }

    public void actioned(final DomainEvent event) {
        channels.actioned().send(MessageBuilder.withPayload(event).build());
    }
}
