package com.example.cloud.avro;

import com.example.cloud.avro.infrastructure.Channels;
import com.github.tomakehurst.wiremock.junit.WireMockClassRule;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.WireMockSpring;
import org.springframework.cloud.stream.test.binder.TestSupportBinder;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import za.brinkc.schema.pojo.DomainEvent;

import java.util.Date;
import java.util.concurrent.BlockingQueue;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.post;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.junit.Assert.assertEquals;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("mock")
public class ApplicationIntegrationMockedTest {

    @Autowired
    private TestSupportBinder testSupportBinder;

    @Autowired
    private Channels channels;

    @Before
    public void cleanup() {
        testSupportBinder.messageCollector().forChannel(channels.actioned()).clear();
    }

    @ClassRule
    public static WireMockClassRule wiremock = new WireMockClassRule(
            WireMockSpring.options().port(8990));

    @Test
    public void givenIncomingEvent_thenHandle_expectOneMessageInActionedTopic() {
        wiremock.stubFor(post(urlEqualTo("/subjects/domainevent/versions"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"id\":117}")));

        wiremock.stubFor(post(urlEqualTo("/subjects/domainevent"))
                .willReturn(aResponse()
                        .withHeader("Content-Type", "application/json")
                        .withBody("{\"subject\":\"domainevent\",\"version\":11,\"id\":117,\"schema\":\"{\\\"type\\\":\\\"record\\\",\\\"name\\\":\\\"DomainEvent\\\",\\\"namespace\\\":\\\"za.brinkc.schema.pojo\\\",\\\"fields\\\":[{\\\"name\\\":\\\"type\\\",\\\"type\\\":[\\\"null\\\",{\\\"type\\\":\\\"string\\\",\\\"avro.java.string\\\":\\\"String\\\"}],\\\"default\\\":null}],\\\"_topic\\\":\\\"actioned\\\"}\"}")));

        DomainEvent event = getDomainEvent();

        channels.events().send(new GenericMessage<>(event));

        BlockingQueue<Message<?>> actionedEvents = testSupportBinder.messageCollector().forChannel(channels.actioned());

        assertEquals("Expected one actioned event", 1, actionedEvents.size());
    }

    public DomainEvent getDomainEvent() {
        DomainEvent domainEvent = DomainEvent
                .newBuilder()
                .setOccuredAt(new Date().getTime())
                .setType("test")
                .build();

        return domainEvent;
    }

}
