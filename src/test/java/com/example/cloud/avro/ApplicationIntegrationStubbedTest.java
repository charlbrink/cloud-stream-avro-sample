package com.example.cloud.avro;

import com.example.cloud.avro.infrastructure.Channels;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.stream.test.binder.TestSupportBinder;
import org.springframework.context.annotation.Import;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import za.brinkc.schema.pojo.DomainEvent;

import java.util.Date;
import java.util.concurrent.BlockingQueue;

import static org.junit.Assert.assertEquals;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("stub")
@Import(SchemaRegistryStubbedTestConfiguration.class)
public class ApplicationIntegrationStubbedTest {

    @Autowired
    private TestSupportBinder testSupportBinder;

    @Autowired
    private Channels channels;

    @Before
    public void cleanup() {
        testSupportBinder.messageCollector().forChannel(channels.actioned()).clear();
    }

    @Test
    public void givenIncomingEvent_thenHandle_expectOneMessageInActionedTopic() {
        DomainEvent event = getDomainEvent();

        channels.events().send(new GenericMessage<>(event));

        BlockingQueue<Message<?>> actionedEvents = testSupportBinder.messageCollector().forChannel(channels.actioned());

        assertEquals("Expected one actioned event", 1, actionedEvents.size());
    }

    public DomainEvent getDomainEvent() {
        DomainEvent domainEvent = DomainEvent
                .newBuilder()
                .setOccuredAt(new Date().getTime())
                .setType("test")
                .build();

        return domainEvent;
    }

}
